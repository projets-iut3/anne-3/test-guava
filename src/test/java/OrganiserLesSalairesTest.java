import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import com.google.common.collect.Multimap;
import com.google.common.collect.ArrayListMultimap;


import java.util.Arrays;

public class OrganiserLesSalairesTest {
    Salaire salaire1;
    Salaire salaire2;
    Salaire salaire3;
    Entreprise e;
    @BeforeEach
    //remplir creer 3 salaires et les saisir dans une entreprise
    void setUp() {
        salaire1 = new Salaire("Doe", "John", 1, 1450.0);
        salaire2 = new Salaire("Doe", "Jane", 2, 2110.0);
        salaire3 = new Salaire("Doe", "Jim", 3, 3540.0);
        e = new Entreprise("nomEntreprise", "ad", "tel", "mel");
        e.addSalaire(salaire1);
        e.addSalaire(salaire2);
        e.addSalaire(salaire3);
    }
    @AfterEach
    //vider les salaires
    void tearDown() {
        // Suppression des salaires
        salaire1 = null;
        salaire2 = null;
        salaire3 = null;
        e = null;
    }

    @Test
    void testBornesSalaires() {
        Assertions.assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> salaire1.setSalaire(-1.0)),
                () -> assertThrows(IllegalArgumentException.class, () -> salaire2.setSalaire(100000.0)),
                () -> assertDoesNotThrow(() -> salaire3.setSalaire(4.0)),
                () -> assertThrows(NullPointerException.class, () -> salaire1.setSalaire(null))
        );
    }

    @Test
    void testNullPrime() {
        Assertions.assertAll(
                () -> assertThrows(NullPointerException.class, () -> salaire1.setPrime(null))
        );
    }

    @Test
    void testNullAnciennete() {
        Assertions.assertAll(
                () -> assertThrows(NullPointerException.class, () -> salaire1.setAnciennete(null))
        );
    }

    @Test
    public void salairesRangeParMontant() throws Exception{
        assertEquals(Arrays.asList(salaire3, salaire2, salaire1) ,
                this.e.rangerSalairesOrdreDecroissant());
    }

    @Test
    public void salairesAvecNom(){
        Multimap<String,Double> lessalaires = ArrayListMultimap.create();
        lessalaires.put("Doe John", 1450.0*0.7);
        lessalaires.put("Doe Jane",2110.0*0.7);
        lessalaires.put("Doe Jim", 3540.0*0.7);
        assertEquals(lessalaires, this.e.recupererSalaireParEmploye());
    }

    @Test
    public void salairesSuperieur1500(){
        assertEquals(Arrays.asList(
                        salaire3),
                this.e.RecupererSalaireSuperieur1500());
    }
}