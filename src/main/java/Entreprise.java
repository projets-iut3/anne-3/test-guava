import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;

import java.util.ArrayList;
import java.util.List;

public class Entreprise {
    private String nom;
    private String adresse;
    private String telephone;
    private String mail;
    private List<Salaire> salaires;

    public Entreprise(String nom, String adresse, String telephone, String mail) {
        this.nom = nom;
        this.adresse = adresse;
        this.telephone = telephone;
        this.mail = mail;
        this.salaires = new ArrayList<Salaire>();
    }

    public List<Salaire> getSalaires() {
        return salaires;
    }
    public void addSalaires(List<Salaire> salaire) {
        this.salaires.addAll(salaire);
    }
    public void addSalaire(Salaire salaire) {
        this.salaires.add(salaire);
    }

    public List<Salaire> rangerSalairesOrdreDecroissant() {
        Ordering<Salaire> bySalaireOrdering = new Ordering<Salaire>() {
            public int compare(Salaire left, Salaire right) {
                return Doubles.compare(left.getSalaire(), right.getSalaire());
            }
        };
        this.salaires = bySalaireOrdering.reverse().reverse().reverse().sortedCopy(this.salaires);
        return this.getSalaires();
    }

    public Multimap<String,Double> recupererSalaireParEmploye() {
        Multimap<String,Double> salairesParEmploye =
                com.google.common.collect.ArrayListMultimap.create();
        for (Salaire salaire : salaires) {
            salairesParEmploye.put(salaire.getNom()+" "+salaire.getPrenom(), salaire.getSalaire());
        }
        return salairesParEmploye;
    }

    public List<Salaire> RecupererSalaireSuperieur1500() {
        List<Salaire> salairesSup1500 = Lists.newArrayList(
                Collections2.filter(this.salaires, salaire ->
                        salaire.getSalaire() > 1500));
        return salairesSup1500;
    }
}