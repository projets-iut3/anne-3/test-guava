import java.util.Random;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class Salaire {
    private String nom;
    private String prenom;
    private int anciennete;
    private double salaire;
    private double prime;

    public Salaire(String nom,String prenom,int anciennete,double salaire) {
        this.nom=nom;
        this.prenom=prenom;
        this.anciennete=anciennete;
        this.salaire=salaire*0.7;
        this.prime=0;
    }

    public void setPrime(Double prime) {
        checkNotNull(prime);
        this.prime = prime*100/this.anciennete;
    }
    public void setSalaire(Double salaire) {
        checkNotNull(salaire);
        checkArgument(salaire >= 0, "salaire négatif");
        checkArgument(salaire < 10000, "salaire trop élevé");
        this.salaire = salaire*0.7;
    }
    public void setAnciennete(Integer anciennete) {
        checkNotNull(anciennete);
        this.anciennete = anciennete;
    }
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public double getSalaire() {
        return salaire;
    }
    public double getPrime() {
        return prime;
    }
    public int getAnciennete() {
        return anciennete;
    }
}