import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multiset;
import com.google.common.io.Files;

import javax.sql.rowset.Joinable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LireMonFichier {
    public void lireFichiercorrompu() throws IOException {
        File file = new File("/home/quentin/IdeaProjects/SujetTPGuava/conversationCorrompu.txt");
        ImmutableList<String> phrases = Files.asCharSource(file, Charsets.UTF_8).readLines();
        List<String> phrasesOrdonne = new ArrayList<>();
        phrasesOrdonne.add(phrases.get(0));
        phrasesOrdonne.add(phrases.get(7));
        phrasesOrdonne.add(phrases.get(4));
        phrasesOrdonne.add(phrases.get(6));
        phrasesOrdonne.add(phrases.get(5));
        phrasesOrdonne.add(phrases.get(1));
        phrasesOrdonne.add(phrases.get(3));
        phrasesOrdonne.add(phrases.get(2));
        File fileToSave = new File("conversationDeCorrompu.txt");
        Files.asCharSink(fileToSave, Charsets.UTF_8).write(joinListToString(phrasesOrdonne));
    }

    public void lireFichierconversation() throws IOException{
        File file = new File("/home/quentin/IdeaProjects/SujetTPGuava/mailCorrompu.txt");
        String content = Files.asCharSource(file, Charsets.UTF_8).read();
        content = CharMatcher.is('@').replaceFrom(content, "e");
        content = CharMatcher.is('%').replaceFrom(content, "i");
        content = CharMatcher.is('+').replaceFrom(content, "s");
        content = CharMatcher.is('^').replaceFrom(content, "o");
        File fileToSave = new File("MailDecorrompu.txt");
        Files.asCharSink(fileToSave, Charsets.UTF_8).write(content);
    }

    private static <T> ArrayList<T> iterableToList(Iterable<T> iterable) {
        //Throw non implemented exception
        return null;
    }

    private static String joinListToString(List<String> list) {
        return Joiner.on('\n').skipNulls().join(list);
    }

    public static void main(String[] args) throws IOException {
        LireMonFichier lireMonFichier = new LireMonFichier();
        lireMonFichier.lireFichiercorrompu();
        lireMonFichier.lireFichierconversation();
    }
}
